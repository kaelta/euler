Program Problem9;
{$mode ObjFPC}

uses crt, math;

(* Problem 9  |   https://projecteuler.net/problem=9
-----------------------------------------------------

  A Pythagorean triplet is a set of three natural numbers:

      a < b < c,

  for which,
  
      a^2 + b^2 = c^2
  
  For example,
      
      (3^2 + 4^2) = (9 + 16 = 25) = 5^2.
  
  There exists exactly one Pythagorean triplet for which a + b + c = 1000.
  Find the product abc.

*)

const x : int32 = 1000; {When a * b * c}
label success;

function pytriplet(a, b: int32): int32;
{Our actual triplet function is thankfully quite easy.
Simply put: c^2 = a^2 + b^2 <3}
begin
  pytriplet := (a ** 2 + b ** 2);
end;

var 
  a : uint32;
  b : uint32;
  c : uint32;

begin

  for a := 1 to x do
  begin

    for b := 1 to a do
    begin

      {Remember the pythagorean rule from before?}
      c := pytriplet(a, b);
      if c = x then {If we've found our condition, we've succeeded <3}
      begin
        goto success;
      end;

    end;

  end;

success: 
  writeln('RESULT: ', a*b*c);
  writeln('Press any key to continue...'); {mew,,}
  readkey;
end.
