--|-------------------------------------------------------------
--|    Problem 3   |  https://projecteuler.net/problem=3
--|-------------------------------------------------------------
--|
--|             The prime factors of 13195 are:
--|
--|                   5, 7, 13 and 29.
--|
--| What is the largest prime factor of the number 600851475143 ?
--|


Prime = (x) ->
	if x == 1
		false
	elseif x < 4
		true
	elseif x % 2 == 0
		false
	elseif x < 9
		true
	elseif x % 3 == 0
		false
	else
		r = math.floor(math.sqrt(x))
		f = 5
		while f <= r do
			return false if x % f == 0
			return false if x % (f + 2) == 0
			f += 6
	true

factors = (n) ->
	for i = 2, math.sqrt(n)
		if n % i == 0 and Prime(i)
			print(i)

print(factors(600851475143))
