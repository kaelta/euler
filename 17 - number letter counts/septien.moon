-- problem 17	~	https://projecteuler.net/problem=17
--
-- If the numbers 1 to 5 are written out in words: 
-- one, two, three, four, five, then there are 
-- 3 + 3 + 5 + 4 + 4 = 19 letters used in total.
--
-- NOTE: Do not count spaces or hyphens. 
-- For example, 342 (three hundred and forty-two) contains 
-- 23 letters and 115 (one hundred and fifteen) contains 20 letters.
-- The use of "and" when writing out numbers is in compliance with British usage.

-- If all the numbers from 1 to 1000 (one thousand) inclusive were written out in words,
-- how many letters would be used?
-- NOTE: Do not count spaces or hyphens. For example, 342 (three hundred and forty-two) 

class Numbers
	tens: (x) =>
		switch x
			when 1
				print "three"
				3
			when 2
				print "three"
				3
			when 3
				print "five"
				5
	teens: (x) =>
		print "teen => (+4)" if x == 3 || x == 4 || x == 5
		print "teen => (+4)" if x == 6 || x == 7 || x == 8
		print "teen => (+4)" if x == 9

	basics: (x) =>
		return	-- im sick of this already