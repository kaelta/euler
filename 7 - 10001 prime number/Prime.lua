function Prime(x)
	if x == 1 then 
		return false
	elseif x < 4 then
		    return true
	elseif x % 2 == 0 then
		return false
	elseif x < 9 then
		return true
	elseif x % 3 == 0 then
		return false
	else
		r = math.floor(math.sqrt(x))
		f = 5
		while f <= r do
			if x % f == 0 then return false end
			if x % (f + 2) == 0 then return false end
			f = f + 6
		end
	end
	
	return true

end

return Prime
