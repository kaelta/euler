--	Problem 6	https://projecteuler.net/problem=6

--	The difference between the sum of the squares of the first ten natural numbers
--	1^2 + 2^2 + ... + 10^2 = 385
--	(1 + 2 + ... + 10)^2 = 55^2 = 3025
--	and the square of the sum is: 3025 - 385 = 2640
--
--	Find the difference between the sum of the squares 
--	of the first one hundred natural numbers and the square of the sum.

sumsquare = (x) ->
	sum = 0
	for i = 1, x
		sum += i ^ 2
	return sum

squaresum = (x) ->
	sum = 0
	for i = 1, x
		sum += i
	return sum ^ 2

print squaresum(100) - sumsquare(100) 

-- Or just do this for fucks sake lol.
-- -----------------------------------
-- x = 100
-- return print (( x * ( x + 1 ) ) / 2) ^ 2 - ( x * (x + 1) * ( x * 2 + 1 ) ) / 6

