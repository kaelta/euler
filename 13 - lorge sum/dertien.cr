module Dertien
  VERSION = "0.1.0"
  <<-DOC
    Problem 13  ~ https://projecteuler.net/problem=13
    -------------------------------------------------
  	Work out the first ten digits of the sum of the following 
  	one-hundred 50-digit numbers. 
  	(in nums.txt)
  DOC

  class Numparse
    # => takes an input of the filename, digits to be returned & the length of the numbers
    def solve(filename : String, digits : Int16 = 10, length : Int16 = 50)
      sum = 0
      File.each_line(filename, chomp: true) do |line|
        sum += line.to_f64 / (Float64.new(10) ** (length - digits + 10))
      end
      (sum * (Int64.new(10) ** 8)).to_i64
    end
  end

  u = Numparse.new
  p u.cass "nums.txt"
end
