require "big"

module Fortyeight
  VERSION = "0.1.0"
  <<-DOC
		Problem 48	~	https://projecteuler.net/problem=48 (oh god oh fuck)
		-------------------------------------------------
		The series, 1^1 + 2^2 + 3^3 + ... + 10^10 = 10405071317.

		TODO: Find the last ten digits of the series, 1^1 + 2^2 + 3^3 + ... + 1000^1000.

		Pseudocode
		----------
		for every number up to 1000^1000
		add 1^1 + 2^2 etc etc
	DOC

  class Fortyeight
    def initialize(@addsum = 0)
    end

    def summation(limit)
      addsum, i = BigInt.new, 0
      loop do
        break if i > limit
        i += 1
        p addsum += (i &** i)
      end
      addsum
    end

    def recursive(limit : Int16 = 10, index = 1, @addsum = 0)
      return @addsum if index > limit
      recursive(limit, index + 1, addsum + (index &** index))
    end
  end

  v = Fortyeight.new
  # p v.summation 10
  p v.recursive
end
