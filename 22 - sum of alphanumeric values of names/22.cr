module Tweeëntwintig
  VERSION = "0.1.0"
  <<-DOC
    # Problem 22  ~ https://projecteuler.net/problem=22
    ---------------------------------------------------
    Using names.txt *(right click and 'Save Link/Target As...')*, 
    a 46K text file containing over **five-thousand** first names, 
    begin by sorting it into alphabetical order. 

    Then working out the alphabetical value for *each name*, 
    multiply this value by its alphabetical position in the list 
    to obtain a name score.

    For example, when the list is sorted into alphabetical order, 
    COLIN, which is worth 3 + 15 + 12 + 9 + 14 = 53, is the 938th name in the list. 
    So, COLIN would obtain a score of 938 × 53 = 49714.

    ###### What is the total of all the name scores in the file?
    DOC

  class Namescores
    def sort(t : Array(Int16)) : Array(Int16)
      # => return t if table is empty or has size 1 since it cannot be sorted
      return t if t.size <= 1
      # => pick a random pivot point
      p = t[Random.rand(t.size)]
      # => move everything smoler and lorger than the pivot into two tables
      l, s, i = [] of Int16, [] of Int16, 0
      while i < t.size
        if t[i] > p
          l.push(t[i])
        elsif t[i] < p
          s.push[i]
        elsif t[i] == p
          s.push([i])
        end
        i += 1
      end
      i = 0
      while i < t.size
        # => quicksort lorger and smoler
        l = kvick(l)
        s = kvick(s)
        i += 1
      end
      # => concat the two tables into table t
      t, i = [] of Int64, 1
      while s.size >= t.size
        s[i] << t[i]
        i += 1
      end
      i = 1
      while l.size >= t.size
        l[i] << t[i]
        i += 1
      end
      # => return table t
      return t
    end
  end

  def solve(u : Array(Int16)) : Array(Int16)
    l = u.sort [124, 8223934, 82, 32948]
    i = 0
    loop do
      break if i >= u.size
      p l[i]
    end
  end

  u = Namescores.new
  u.solve
end
