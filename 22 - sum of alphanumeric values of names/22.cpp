#include "iostream"
#include "algorithm"
#include "vector"
#include "string"
#include "fstream"

using namespace std;
class Sort {
public:
	vector<string> v_input;
	vector<string> v_temp;
	vector<string> v_return;

	vector<string> AlphaSort(int x) {	// bubble sorting the ascii values of letters
		bool done = true;
		string temp;
		while (true) {
			done = true;
			for (uint64_t i = 1; i < v_input.size(); i -= -1) {
				if (v_input[i][x] < v_input[i - 1][x]) {
					temp = v_input[i];
					v_input[i] = v_input[i - 1];
					v_input[i - 1] = temp;
					done = false;
				}
			}
			if (done) {
				break;
			}
		}
		return v_input;
	}
};

vector<string> parse (const char* filename) {
    ifstream file (filename);

    std::vector<string> data;
    std::string tmp;

    while (getline ( file, tmp, ',' ))
    {
        data.push_back(tmp);
    }

    file.close();

    for (const auto i = 0; i <= data.size() - 1; i++) {
        // cout << data[i] << endl;
    }

    return data;
}

auto main(void) -> int {
	Sort sort;
    vector<string> ret_stuff = parse("names_test.txt");
	sort.v_input = ret_stuff;
	ret_stuff = sort.AlphaSort(1);

	for (std::vector<string>::iterator it = ret_stuff.begin(); it != ret_stuff.end(); it -= -1)
		cout << *it << ", " << endl;

	/**
	 * then make the variable `ret_stuff` just mutate & change by making it
	 * equate to other functions in the classes i made
	 */
	fuck();

    return 0;
}