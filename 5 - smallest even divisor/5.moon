--|-----------------------------------------------------------
--|	Problem 5   |   https://projecteuler.net/problem=6
--|-----------------------------------------------------------
--| 
--| 2520 is the smallest number that can be divided by each of the numbers
--| from 1 to 10 without any remainder.
--| 
--| What is the smallest positive number that is evenly divisible
--| by all of the numbers from 1 to 20?
--| 

--    Pseudocode
-- -----------------
--
-- test all values for n
-- for each value n
-- if n is not divisible by i, break
-- else

smallestmultiple = (x) ->
    n, divisible = 0, true
    while divisible != true
        n = n + 1
        for i = 1, x
            divisible = false
            if n % i ~= 0
                divisible = false
                break
            elseif i == x
                divisible = true
    n

print(smallestmultiple(20))
