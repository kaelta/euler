--|-----------------------------------------------------------
--|	Problem 4   |   https://projecteuler.net/problem=4
--|-----------------------------------------------------------
--| A palindromic number reads the same both ways.
--| The largest palindrome made from the product of two 2-digit numbers is:
--|
--|                   9009 = 91 × 99.
--|
--| Find the largest palindrome made from the product of two 3-digit numbers.
--|

pallindromic = (x) ->
	if tostring(x) == string.reverse(x) 
		true
	false


pallindrome = (test, limit) ->
	largest = 0
	for i = test, limit
		for j = test, i
			num = i * j
			if pallindromic i * j and num > largest
				largest = num
				--print "i: ", i, "j: ", j, "sum: ", i * j, "\n"

	largest

print pallindrome 100, 999
